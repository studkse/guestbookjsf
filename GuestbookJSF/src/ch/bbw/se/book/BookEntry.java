package ch.bbw.se.book;
import javax.faces.bean.*;

import java.awt.print.Book;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
/**
 * Book Entry
 * @author 5im17saennin
 * @version 31.10.2018
 */
@ManagedBean(name="bE")
@SessionScoped 

public class BookEntry {
	
	private String message;
	private String dateString;
	private User user;
	
	//Constructors
	public BookEntry() {
		Date d = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("E hh:mm:ss dd.MM.yyyy");
		dateString = dateFormat.format(d);
	}
	public BookEntry(String message, User user) {
		this();
		this.user = user;
		this.message = message;
	}
	//Getters and Setters
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDateString() {
		return dateString;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}

package ch.bbw.se.book;
import javax.faces.bean.*;

import java.awt.print.Book;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
/**
 * Login Bean
 * @author 5im17saennin
 * @version 31.10.2018
 */
@ManagedBean(name="logB")
@SessionScoped 

public class LoginBean {
	


	private String message;
	private Date date;
	private String userID;
	private String logName;
	private String pw;
	private String err;
	
	// Getters and Setters
	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getLogName() {
		return logName;
	}

	public void setLogName(String logName) {
		this.logName = logName;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		this.err = err;
	}
	
	//methods
	public String login() {
		
		GuestBookBean gbb = 	(GuestBookBean)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("gbb");
		gbb.getU
		
		//ok
		return "guestBook.xhtml";
		
		
	}


}

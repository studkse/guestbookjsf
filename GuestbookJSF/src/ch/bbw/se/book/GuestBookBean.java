package ch.bbw.se.book;
import javax.faces.bean.*;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
/**
 * 
 * @author 5im17saennin
 * @version 31.10.2018
 */
@ManagedBean(name="gbb")
@SessionScoped 

public class GuestBookBean {
	
	private String welcome = "Willkommen Zum Guestbook";
	private BookEntry entry;
	private ArrayList<BookEntry> entries;
	
	private ArrayList<User> users;
	private User user;
	
	//Constructor
		public GuestBookBean() {
			entries = new ArrayList<BookEntry>();
			entry = new BookEntry();
			entries.add(new BookEntry("Seems to be a guestbook", new User("Max", "myPW")));
		}
	//Getters and Setters
	public String getWelcome() {
		return welcome;
	}
	
	public void setWelcome(String welcome) {
		this.welcome = welcome;
	}
	
	public BookEntry getEntry() {
		return entry;
	}
	public ArrayList<BookEntry> getEntries(){
		return entries;
	}
	//methods
	public String newEntry() {
		entries.add(new BookEntry(entry.getMessage(), new User("Max", "myPW")));
		return null;
	}
	public String deleteEntry(BookEntry entry) {
		entries.remove(entry);
		return "";
	}
}
